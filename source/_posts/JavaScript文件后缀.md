---
title: JavaScript文件后缀
tags: [随笔,前端]
date: 2020-12-22 22:01:21
---

# JavaScript文件后缀

## 一般都为 .js 后缀
按照惯例，JavaScript代码文件的扩展名名.js。但从实际情况来看，胆码的名称可以使用任何扩展名，浏览器都会把其中的内容当做JavaScript代码来解释。
## 放在script标签里
 src属性 和 写在其中的代码 只能选一个。
 即，要么用src属性导入，要么在其中写入代码。

