---
title: JavaScript6-String
tags: [js,前端]
date: 2021-01-14 19:53:00
category: [js,前端]
---
## 字符串
字符串： 由特定字符集（ASCII、Unicode等）里的字符所组成的序列。
### 声明

```javascript
//例子
var str = "string";
```

<!--more-->

### 常用转义字符
|转义序列|代表字符|
|---|---|
|\t|制表符|
|\n|换行|
|\\"|"|
|\\'|'|
|\\\\|\\|
### 字符串属性
> .length 长度
### 字符串方法
|方法|作用|
|---|---|
|concat()|连接字符串，不改变原字符串|
|indexOf()|查找字符串，返回索引 *从0开始* |
|lastIndexOf()|返回最后出现的字符串索引|
|repeat(n)|重复字符串n次|
|replace(now,new)|替换字符串|
|split()|将字符串分解为数组|
|substr()|切割提取字符串|
|toLowerCase()|把字符串全部转换为小写|
|toUpperCase()|把字符串全部转换为大写|

### 模板字符串

通过将变量名包含在${ 。。。 }中来引用字符串。
如：
```javascript
var a = "my blog";
// 注意 模板字符串必须位于 反单引号 之间。
var c = `a String of ${a}.`;
console.log(c);
```
输出：
```shell
[Running] node "e:\test.js"
a String of my blog.
```
