---
title: 终端机常用命令
date: 2020-12-11 16:26:03
tags: [随笔]
category: 随笔
---
# 终端机常用命令

|Windows|macOS/Linux|作用|
|:----:|:------:|------|
|cd|cd|切换目录|
|cd|pwd|获取当前所在目录|
|dir|ls|列出当前的文件列表|
|mkdir|mkdir|创建新的目录|
|无|touch|创建文件|
|copy|cp|复制文件|
|move|mv|移动文件|
|del|rm|删除文件|
|cls|clear|清屏|
