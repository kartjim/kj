---
title: hexo插入图片
tags: [随笔,hexo]
category: 随笔
date: 2020-12-24 22:16:39
---
# hexo插入图片
![sky](/images/clouds.jpg)
>将图片放在 source/images 文件夹中，
然后通过 \!\[ \]\(/images/image.jpg\) 的方法访问它们。
>