---
title: javascript3-日期和时间
tags: [js,前端]
date: 2021-01-09 10:26:07
category: [js,前端]
---
## Date对象
JavaScript中时间和日期对象和java类似，都是Date对象。且都需要进行创建Date对象才能使用。
### 当前时间和日期
新建一个Date对象：
```javascript
var date = new Date();
```
date就是一个Date对象，包含创建对象时的日期和时间信息。
date具有一些可以操作日期和时间的方法，如：

<!--more-->

#### 获取年月日
```javascript
var date = new Date();
//FullYear();是四位数的年份，如2021;而year();是二位数的年份；为了不引起舞蹈，一般使用FullYear();
var year = date.getFullYear(); 
//获取的月份是从0-11的数字，需要加上1才是真正的月份，如11表示12月。
var month = date.getMonth();
//获取日期，1-31
var dates = date.getDate();
//获取星期，0-6，0表示周日，其它正常。
var day = date.getDay();
```
#### 获取几点几分
```javascript
//
var hours= date.getHours();
//
var minute = date.getMinutes();
//
var seconds = date.getSeconds();
//
```
#### 得到当前具体时间
```javascript
console.log(year+"-"+month+"-"+dates+" "+hour+":"+minute+":"+seconds);
```
执行结果：
```shell
[Running] node "e:\hexo\kartjim\test.js"
2021-1-9 10:57:16
```

### 创建指定日期和时间的Date对象
创建Date对象时，给Date对象传参即可创建指定日期和时间的Date对象。
```javascript
//指定从1970-1-1到程序执行时的毫秒数
new Date(ms) 
```
或者
```javascript
//使用日期字符串
new Date(String-date)
//比如：
new Date("January  9,2021 11:09:16");
```
或者
```javascript
//使用每个参数
new Date(year,month,day,hour,minute,seconds,ms);
// 靠后的参数可以省略，默认为0。
```

### 编辑Date对象
#### 设置自定义时间和日期
```javascript
//更改 为当月23号
date.setDate(23);
```
其它参数同理。
注意：
- **设置新的日期之后，之前获取的日期不会变化！**
需要重新获取 dates = date.getDate();
或者直接输出 date.getDate()

- _设置月份的时候要注意，因为月份并不是真正的月份，而是小一个数！_
- 更改月份或者日期时，星期几是由设置之后决定的（设置前获取的星期不是）。

#### 转换格式输出
手动控制日期和时间的输出顺序
```javascript
console.log(year+"-"+month+"-"+dates+" "+hour+":"+minute+":"+seconds);
```
要先获得各个参数，还要自己排序输出，比较麻烦。
```shell
[Running] node "e:\hexo\kartjim\test.js"
2021-1-23 11:16:34
```
利用 `toDateString()` 方法可以把日期转换格式，并不需要自己一个个获取单个参数，直接输出可以更容易理解的日期。
```javascript
console.log(date.toDateString());
```
```shell
[Running] node "e:\hexo\kartjim\test.js"
Sat Jan 23 2021
```
同理，`toTimeString()`是另外一种方式。
```javascript
console.log(date.toTimeString());
```
```shell
[Running] node "e:\hexo\kartjim\test.js"
11:16:34 GMT+0800 (GMT+08:00)
```