---
title: HTML5文档的基本结构及创建方法
date: 2020-12-13 10:22:57
tags: [前端,html]
category: 前端
---
# HTML5文档基本结构
## 大纲
如果用专门的软件新建一个html文档，会自动出现以下大纲，这是HTML5的基本结构。这些是必须要写的内容。
```html
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>标题</title>
</head>

<body>
<h1>一级标题</h1>
这里填写具体文本内容
</body>
</html>
```
## 说明
<!doctype html>是文档类型说明，相对于其它HTML标准，标准5的文档类型说明是相对简单的！
# html文件的创建
众所周知，HTML用普通的文本编辑器就可以编写，如Windows自带的记事本就可。
步骤：

- 创建一个文件.txt，用记事本打开。
- 写好html代码之后选择另存为；
- 将文档后缀名改为.html或者.htm都可以，编码选择UTF-8；
- 保存好之后双击即可打开web查看；

博客来自：[纪大眼袋](https://blog.csdn.net/qq_46590483/article/details/106972610)