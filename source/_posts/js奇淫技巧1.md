---
title: js奇淫技巧1
tags: [js,前端]
date: 2021-01-02 16:30:37
category: [js,前端]
---
## 函数
### 变量作用域
- var

JavaScript有两种作用域：全局 / 局部作用域
>var声明的变量 在函数内部声明的变量只存在于函数内部

>用 this 可以调用全局作用域
- let

<!--more-->

为了实现变量块级作用域，js用一个`let`关键字声明具有严格块级作用域的变量。

- const

const关键字用于声明一个常量，使常量不能被重新赋值或重新声明。
### 箭头函数
先看一般的匿名函数：
```javascript
var sayHello = function(){ alert("Hello");};
```
箭头函数（arrow function）可以使匿名函数更加简洁：
```javascript
var sayHello = () => alert('Hello');
```
> 只有包含多个表达式，才需要花括号。

```javascript
myFunc = (x,y) => {
     x += y;
     alert(x); 
     }
```
>=>符号通常被称为胖箭头符号

### 默认参数
要指定一个默认参数，只要给函数定义中的参数一个默认值就行。
例如：
```javascript
function warn(temp,subtitle='--warning'){
    alert(subtitle+" , "+temp+" ;");
}
```
**在函数定义中，一般将有默认值的参数，放在没有默认值的参数后面**