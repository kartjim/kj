---
title: js奇淫技巧2
tags: [js,前端]
date: 2021-01-03 10:38:21
category: [js,前端]
---
## DOM

```mermaid
graph LR
A[window] --> B[document]
A --> C[location]
A --> D[history]
A --> E[navigator]
A --> F[... ...]
```

层级数最顶端的是浏览器window对象，它是页面的DOM表示中一切对象的父对象。
window对象有一些子对象：
### document对象
任何HTML页面都会创建一个document对象，包含全部HTML内容和其它构成页面的资源。
```javascript
document.write()
```

<!--more-->
## 与用户交互
用  .  调用子对象
```javascript
document.body
window.alert
...
```
### window对象下的常用方法
*调用window对象下的方法可省略 window ；
即 将 `window.alert();` 简写为 `alert();`*
```javascript
alert();
confirm(); //有选择的两个按钮
prompt(); //允许输入信息，并作为返回值
```
### document对象下的常用方法
#### 获取标签节点
```javascript
1. getElementById(id)
2. getElementsByName(name) 
3. getElementsByTagName(tagname) 
```
#### innerHTML
可通过节点的 innerHTML 属性来访问文本节点的值，可以向HTML里写入文字。
HTML：
```html
<div id="textbox">
    <p>your txt</p>
</div>
```
```javascript
var myDom = document.getElementById("textbox").innerHTML = "<p>更改后的内容</p>"
```
### history对象下的常用方法
history对象只有一个属性，就是它的长度，代表用户访问过的页面的数量。
>history.length
>
history对象有三个方法：forward()、backward()、go()。
- forward()、backward()对应单页浏览器中前进和后退。
- go()跳转到历史记录里的相对位置，正数是前进，负数是后退。

### location对象下的常用方法
location对象包含当前加载页面的URL信息。
比如：http://www.baidu.com
一般组成部分：
>[协议]//[主机名]:[端口]/[路径][搜索][hash]
```javascript
location.href = 'http://www.baidu.com'; //直接设置对象的herf属性
location.replace('http://www.baidu.com');// 替换旧的URL
location.reload ();  //刷新页面
```
### navigator对象下的常用方法
navigator对象包含了浏览器 程序本身的数据。
如：
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div>
        <p></p>
    </div>
    <script>
        //输出浏览器名字,可能会输出错误的浏览器名字！
        document.write(navigator.appCodeName);
    </script>
</body>
</html>
```