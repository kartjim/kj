---
title: CKEditor
tags: editor
date: 2021-01-31 21:05:35
category: editor
---
## CKEditor集成
<script src="https://cdn.ckeditor.com/ckeditor5/25.0.0/inline/ckeditor.js"></script>
<h1>Inline editor</h1>
<div id="editor">
    <p>This is some sample content.</p>
</div>
<script>
    InlineEditor
        .create( document.querySelector( '#editor' ) )
        .catch( error => {
            console.error( error );
        } );
</script>