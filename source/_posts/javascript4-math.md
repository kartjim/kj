---
title: javascript4-math
tags: [js,前端]
date: 2021-01-14 18:55:48
category: [js,前端]
---
## javascript之Math对象
_与Date对象不同的是，Math对象不需要创建就可以使用_
### math常用方法
|方法|描述|
|---|---|
|ceil(n)|向上取整|
|floor(n)|向下取整|
|max(a,b...)|返回最大值|
|min(a,b,...)|返回最小值|
|round(n)|返回一个四舍五入的值|
|random()|返回一个0-1之间的随机数|

### 数学常数（Math属性）
|常数|值|
|---|---|
|E|e≈2.718 ...|
|LN2|ln2|
|LN10|ln10|
|LOG2E|以2为底e的对数|
|LOG10E|以10为底e的对数|
|PI|π|
|SQRT1_2|2的平方根的倒数|
|SQRT2|2的平方根|